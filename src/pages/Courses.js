import React from 'react';
import Course from '../components/Course';

import coursesData from '../data/courses';

export default function Courses(){
	//create multiple Course components corresponding to the number of courses in our courseData
	const courses = coursesData.map(courseData => {
		return(
			//pass data to the course component via props
			<Course key={courseData.id} course={courseData}/>
		)
	})

	return(
		<React.Fragment>
			{courses}
		</React.Fragment>
	)
}