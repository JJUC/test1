import React from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/NavBar'
// import Home from './pages/Home'
import Courses from './pages/Courses'
import { Container } from 'react-bootstrap'

export default function App() {
  return (
    <React.Fragment>
      <NavBar />
      <Container>
      	<Courses />
      </Container>
    </React.Fragment>
  );
}
