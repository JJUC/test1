import React from 'react'
import { Card, Button } from 'react-bootstrap'

export default function Course({course}){
	const { name, description, price } = course;
	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<h3>Description:</h3>
					<p>{description}</p>
					<h4>Price:</h4>
					<p>PHP {price}</p>
				</Card.Text>
				<Button variant="primary">Enroll</Button>
			</Card.Body>
		</Card>
	)
}